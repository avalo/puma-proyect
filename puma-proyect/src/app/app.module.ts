import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import {RouterModule, Routes} from '@angular/router';

import { AppComponent } from './app.component';
import { HeaderComponent } from './component/header/header.component';
import { UsuariosComponent } from './component/usuarios/usuarios.component';
import { EmpresasComponent } from './component/empresas/empresas.component';
import { ServiciosComponent } from './component/servicios/servicios.component';
import { FooterComponent } from './component/footer/footer.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { HomeComponent } from './component/home/home.component';
import { ProyectosComponent } from './component/proyectos/proyectos.component';
import { ContactoComponent } from './component/contacto/contacto.component';
import { NosotrosComponent } from './component/nosotros/nosotros.component';

const rutas: Routes = [
  {path:'', component: HomeComponent},
  {path:'proyectos', component: ProyectosComponent},
  {path:'servicios', component: ServiciosComponent},
  {path:'contactanos', component: ContactoComponent},
  {path:'nosotros', component: NosotrosComponent}
];
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    UsuariosComponent,
    EmpresasComponent,
    ServiciosComponent,
    FooterComponent,
    HomeComponent,
    ProyectosComponent,
    ContactoComponent,
    NosotrosComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(rutas),
    NgbModule,
    FontAwesomeModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
